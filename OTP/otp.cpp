#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define KEY_LENGTH 15 

int main(){
  unsigned char ch;
  FILE *fpIn, *fpOut;
  int i;
  unsigned char key[KEY_LENGTH];

  fpIn = fopen("input.txt", "r");
  fpOut = fopen("output.txt", "w");

	srand(time(NULL));

	/*Key generation*/
  fprintf(fpOut, "%s", "Key:");
  for (unsigned j=0; j< KEY_LENGTH; j++)	
  {
	  key[j] = rand() % 256;
	  fprintf(fpOut, "%02X", key[j]);
	  }

	/*Ciphertext generation*/
	fprintf(fpOut, "%s", "\nChipertext:");
  i=0;
  fprintf(fpOut, "%c", '\n');
  while (fscanf(fpIn, "%c", &ch) != EOF) {
	if (ch!='\n') {
      fprintf(fpOut, "%02X", ch ^ key[i]); 
      i++;
      if (i > KEY_LENGTH) {
		  fprintf(fpOut, "%s", "Error!!!!!!!!!!!!"); 
		  }
      }
      /*new line means new message, hence we reuse the key*/
    else {
		i = 0;  
		fprintf(fpOut, "%c", '\n');
		}
    }
 
  fclose(fpIn);
  fclose(fpOut);
  return 1;
} 
